<?php

namespace Drupal\ebt_accordion\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ebt_core\Plugin\Field\FieldWidget\EbtSettingsDefaultWidget;

/**
 * Plugin implementation of the 'ebt_settings_accordion' widget.
 *
 * @FieldWidget(
 *   id = "ebt_settings_accordion",
 *   label = @Translation("EBT Accordion settings"),
 *   field_types = {
 *     "ebt_settings"
 *   }
 * )
 */
class EbtSettingsAccordionWidget extends EbtSettingsDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['ebt_settings']['pass_options_to_javascript'] = [
      '#type' => 'hidden',
      '#value' => TRUE,
    ];

    $element['ebt_settings']['styles'] = [
      '#title' => $this->t('Styles'),
      '#type' => 'radios',
      '#options' => [
        'default' => $this->t('Default'),
        'text_only' => $this->t('Text only'),
        'plus_minus_left' => $this->t('Plus/Minus icons on the left'),
        'plus_minus_right' => $this->t('Plus/Minus icons on the right'),
      ],
      '#default_value' => $items[$delta]->ebt_settings['styles'] ?? 'default',
      '#description' => $this->t('Select predefined styles for accordion.'),
    ];

    $element['ebt_settings']['collapsible'] = [
      '#title' => $this->t('Collapsible'),
      '#type' => 'checkbox',
      '#default_value' => $items[$delta]->ebt_settings['collapsible'] ?? 1,
      '#description' => 'Whether all the sections can be closed at once. Allows collapsing the active section.',
    ];

    $element['ebt_settings']['closed'] = [
      '#title' => $this->t('All closed'),
      '#type' => 'checkbox',
      '#default_value' => $items[$delta]->ebt_settings['closed'] ?? NULL,
      '#description' => $this->t('This requires the collapsible option to be checked'),
    ];

    $element['ebt_settings']['opened'] = [
      '#title' => $this->t('All opened'),
      '#type' => 'checkbox',
      '#default_value' => $items[$delta]->ebt_settings['opened'] ?? NULL,
    ];

    $element['ebt_settings']['active'] = [
      '#title' => $this->t('Active section'),
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->ebt_settings['active'] ?? NULL,
      '#description' => $this->t('The zero-based index of the panel that is active (open). A negative value selects panels going backward from the last panel.'),
    ];

    $element['ebt_settings']['disable'] = [
      '#title' => $this->t('Disable'),
      '#type' => 'checkbox',
      '#default_value' => $items[$delta]->ebt_settings['disable'] ?? NULL,
      '#description' => $this->t('Disables the accordion.'),
    ];

    $element['ebt_settings']['heightStyle'] = [
      '#title' => $this->t('Height style'),
      '#type' => 'radios',
      '#options' => [
        'auto' => $this->t('Auto'),
        'fill' => $this->t('Fill'),
        'content' => $this->t('Content'),
      ],
      '#default_value' => $items[$delta]->ebt_settings['heightStyle'] ?? 'content',
      '#description' => $this->t('Controls the height of the accordion and each panel.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value += ['ebt_settings' => []];
    }
    return $values;
  }

}
